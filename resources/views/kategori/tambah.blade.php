@extends('layout.master')

@section('judul')
Halamantambah kategori
@endsection

@section('content')
<form action="/kategori" method="POST">
    @csrf
  <div class="mb-3">
    <label  class="form-label">nama kategori</label>
    <input type="text" name="nama" class="form-control" >
    
  </div>
  <div class="mb-3">
    <label  class="form-label">description</label>
    <textarea name="descripsi" class="form-control"  cols="30" rows="10"></textarea>
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection